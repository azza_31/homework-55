import React from "react";
import './App.css';
import Burger from './Burger/Burger'

const App = () => {
    return (
        <div>
            <Burger/>
        </div>
    );
}

export default App;
